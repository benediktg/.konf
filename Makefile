default: setup

setup:
	sh/deploy.sh
	sh/check-gc.sh

update:
	git stash
	git checkout master
	git pull origin master
	sh/deploy.sh
	sh/check-gc.sh

local: update
	git checkout local
	git rebase master

clean:
	sh/clean.sh

trackpoint:
	sh/trackpoint-enable.sh

author:
	git checkout master
	sh/set-gc.sh "Benedikt Geißler" "benediktg@openmailbox.org"
