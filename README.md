Meine Dotfiles
==============

für *zsh*, *vim* und *emacs*

Einrichtung
-----------

`cd ~ && git clone https://gitlab.com/benediktg/.konf.git`

`cd ~/.konf && make`

### Anpassung des Namens sowie der E-Mail-Adresse in der .gitconfig:

`sh/set-gc.sh`

### Für Updates:

`cd ~/.konf && make local`

