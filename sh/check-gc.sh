#!/bin/bash

name="$(git config --global user.name)"
email="$(git config --global user.email)"
script="$(dirname "${BASH_SOURCE[0]}")/set-gc.sh)"

if [[ $name == "Your Name" || $email == "you@example.com" ]]; then
	printf "\n\nplease run %s\n\n" "$script"
fi
exit 0
