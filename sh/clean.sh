#!/bin/bash

[[ -f ~/.zshrc ]] && rm ~/.zshrc
[[ -d ~/.zsh-hl ]] && rm -rf ~/.zsh-hl
[[ -e ~/.vim ]] && rm -rf ~/.vim
git submodule -q deinit .

cd "$(dirname "${BASH_SOURCE[0]}")"/../dotfiles || exit

for file in .*
do
	if (( ${#file} > 2 )); then
		[[ -e ~/$file ]] && rm -rf ~/"$file"
	fi
done

exit 0
