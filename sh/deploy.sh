#!/bin/bash

CONF_DIR="$PWD"

if [[ ! -f ~/.zshrc ]]; then
    echo "downloading zsh grml config..."
    wget -qO ~/.zshrc http://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
fi
if [[ ! -d ~/.zsh-hl ]]; then
    echo "downloading zsh syntax highlighting..."
    git clone -q git://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh-hl
fi
[[ -d ~/.cache/zsh ]] || mkdir -p ~/.cache/zsh
[[ -d ~/.vim/colors ]] || mkdir -p ~/.vim/colors
if [[ ! -e ~/.vim/colors/solarized.vim ]]; then
    echo "downloading vim solarized color scheme..."
    wget -qO ~/.vim/colors/solarized.vim https://raw.githubusercontent.com/altercation/vim-colors-solarized/master/colors/solarized.vim
fi
if [[ ! -e ~/.transfer.sh ]]; then
    echo "downloading transfer script..."
    wget -qO ~/.transfer.sh https://gist.githubusercontent.com/nl5887/a511f172d3fb3cd0e42d/raw/transfer.sh
fi
git submodule -q init .
echo "updating git submodules..."
git submodule -q update .

cd "$CONF_DIR"/dotfiles || exit
for file in *; do
    [[ -d "$file" ]] && rm -r ~/."$file"
    ln -sf "$PWD"/"$file" ~/."$file"
    [[ -d "$file"/"$file" ]] && rm "$file"/"$file"
done
cd "$CONF_DIR" || exit

ln -sf "$PWD"/bin ~/.bin
[[ -d bin/bin ]] && rm bin/bin

exit 0
