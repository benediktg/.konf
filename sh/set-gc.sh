#!/bin/bash

name="$(git config --global user.name)"
email="$(git config --global user.email)"

if (( $# == 2 )); then
	name="$1"
	email="$2"
elif [[ "$1" == "-i" || $name == "Your Name" && $email == "you@example.com" ]]; then
	echo "Please type in your name:"
	read -r name
	echo "Please type in your email address:"
	read -r email
	if [[ -z "$name" && -z "$email" ]]; then
		exit 0
	fi
	if [[ -z "$name" ]]; then
		name="$(git config --global user.name)"
	fi
	if [[ -z "$email" ]]; then
		email="$(git config --global user.email)"
	fi
elif [[ -n $(git config --global user.name) ]]; then
	if [[ -n $(git config --global user.email) ]]; then
		echo "Name and email are already set."
		exit 0
	fi
else
	echo "Usage:"
	echo "$0 \"name\" \"mail address\""
	echo "$0 -i"
	exit 1
fi

git config --global user.name "$name"
git config --global user.email "$email"

echo "Do you want to create a local branch? (recommended, type 'yes')"
read -r flag
if [[ "$flag" == "yes" ]]; then
	cd ~/.konf || exit
	git checkout -b local
	git commit -am "custom setting"
fi
exit 0
