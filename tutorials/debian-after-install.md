Paketauswahl zum Installieren
=============================

PC und Notebook
---------------

`zsh vim git git-gui gtk2-engines-oxygen vrms plymouth yakuake debian-goodies hplip-gui baloo-kf5 kdeconnect kde-config-baloo-advanced print-manager kde-config-systemd build-essential thermald`

nur Notebook
------------

`tlp`

nur PC
------

`firmware-linux-nonfree`

KDE-spezifisch
--------------

### Wetter-Widget

installieren: `qml-module-qtquick-xmllistmodel`

anschließend Weather Widget aus Internet herunterladen

### ksuperkey

installieren: `libx11-dev libxtst-dev pkg-config`

`mkdir ~/.build`

`git clone https://github.com/hanschen/ksuperkey.git ~/.build/ksuperkey`

`cd ~/.build/ksuperkey`

`sed -i 's/PREFIX=\/usr/PREFIX=\/usr\/local/g' Makefile`

`make`

`sudo make install`

anschließend in Systemeinstellungen → Starten & Beenden ksuperkey als Programm hinzufügen