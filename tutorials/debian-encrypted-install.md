Debian verschlüsselt installieren
=================================

1. Debian-Stick booten
2. graphical expert install
3. Installationsroutine folgen bis *Installer-Komponenten von CD laden*
4. *crypto-dm-modules-…* auswählen
5. wieder Installationsroutine folgen bis *Festplatte partitionieren*
6. **Strg-Alt-F2, Enter** drücken
7. `anna-install crypto-dm-modules cryptsetup-udeb`
8. `cryptsetup luksOpen /dev/sda2 main` (ggf. Partitionsbezeichner ändern, nicht notwendig, wenn noch keine LUKS-Partion vorhanden)
9. mit **Strg-Alt-F5** wieder zum Installer wechseln
10. *weiter* klicken, anschließend *manuell* wählen
11. ggf. bei *LVM konfigurieren* logische Volumes erstellen
12. Partitionierung durchführen, beachten, dass separate /boot-Partition vorliegt sowie bei SSD discard als mount-Option angeben, anschließend Installer bis zur Stelle vor dem Neustarten befolgen
13. wieder mit **Strg-Alt-F2** in die Konsole wechseln
14. `mount /dev/mapper/main-debian /mnt` (auch hier wieder ggf. Partitionsnamen ändern)
15. `mount /dev/sda5 /mnt/boot`
16. `mount /dev/mapper/main-home /mnt/home`
17. `mount --bind /dev /mnt/dev`
18. `mount --bind /sys /mnt/sys`
19. `mount --bind /proc /mnt/proc`
20. `chroot /mnt /bin/bash`
21. `printf "main\tUUID="%s"\tnone\tluks,discard\n" "$(cryptsetup luksUUID /dev/sda2)" | tee -a /etc/crypttab` (das ",discard" dient dazu, im Falle von SSDs den TRIM-Befehl zu unterstützen, Partitionsnamen ggf. ändern)
22. `echo "dm-crypt" >> /etc/modules`
23. `update-initramfs -u -k all`
24. die Datei `/etc/default/grub` bearbeiten und die *GRUB_CMDLINE_LINUX_DEFAULT*-Zeile etwa so verändern: `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash kopt=root=/dev/mapper/main-debian"`
25. `update-grub`
26. `mv /etc/mtab{,.bak}`
27. `ln -s /proc/self/mounts /etc/mtab`
28. `exit`
29. `umount /mnt/*`
30. `umount /mnt`
31. **Strg-Alt-F5** und neustarten

Diese Anleitung basiert auf [dieser](http://www.bobrosbag.nl/index.php/2015/05/28/reinstall-debian-on-an-existing-lukslvm-setup/) und [dieser](https://wiki.ubuntuusers.de/System_verschl%C3%BCsseln).
