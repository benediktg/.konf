```sh
# Packman Essentials hinzufügen
sudo zypper ar http://ftp.halifax.rwth-aachen.de/packman/suse/openSUSE_Tumbleweed/Essentials/ packman-essentials 
# Priorität auf 95 hochsetzen 
sudo zypper mr -p 95 packman-essentials 
# Dist-Upgrade
sudo zypper dup 
# VLC und komplettes Gstreamer installieren
sudo zypper in gstreamer-plugins-bad-orig-addon gstreamer-plugins-ugly-orig-addon vlc vlc-noX-lang chromium-ffmpeg
```